{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `while` loops\n",
    "\n",
    "Another iteration method is a `while` loop. These keep going until a condition is no longer True.\n",
    "\n",
    "```python\n",
    "while some_condition is True:\n",
    "    # Statements that keep executing as long as some_condition is True.\n",
    "    statements\n",
    "```\n",
    "\n",
    "Here's an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5\n",
    "\n",
    "# Do the code inside the loop as long as n > 0\n",
    "while n > 0:\n",
    "    print('n =', n)\n",
    "    n = n - 1 # subtract 1 from n\n",
    "print('Done')\n",
    "print(f'final value of n is {n}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You provide a test at the beginning of the loop and the code inside the loop keeps evaluating until the condition is no longer `True`. In this case, 1 is subtracted from `n` each time through the loop, and it stops when `n` is no longer greater than 0. In fact, the value is exactly equal to 0 at this point. Notice that it never printed `n = 0`, because it bailed out of the loop when that was the case. Then it printed `Done` and the final value of `n`.\n",
    "\n",
    "Again, there's an interactive example at https://usu-python.gitlab.io/control-flow/while_example.html.\n",
    "\n",
    "If the beginning condition is never `True`, then the loop will never execute. In this example, `n` starts at -1, which is not greater than 0, so the indented code under the `while` line never executes. The line that prints `Done` is outside of the loop, so it still runs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = -1\n",
    "while n > 0:\n",
    "    print('n =', n)\n",
    "    n = n - 1\n",
    "print('Done')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `while` loop may also execute indefinitely. This is called in an *infinite loop* and is generally an extremely bad thing. It means your script will never stop, and can potentially use up all of your memory or CPU and crash your computer. You'd end up in this situation if you forgot to subtract 1 from `n` in the first example, because `n` would always be greater than 0.\n",
    "\n",
    "```python\n",
    "n = 5\n",
    "while n > 0:\n",
    "    print('n =', n)\n",
    "```\n",
    "\n",
    "This would print 5 over and over and over again, ad infinitum. If you try it, be prepared to kill your Python process because it'll never stop on its own."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "Use a `while` loop to find the first integer that's divisible by 3 that's greater than 20. To do this, create a variable (I'll call it `x`) and set it to 0. Then use a `while` loop to iteratively add three to `x` as long as the value of `x` less than or equal to 20. If you did it correctly, when the loop stops, `x` will contain the first integer greater than 20 that's divisible by 3. **Print out the final value of `x`.**\n",
    "\n",
    "**If you accidentally find yourself in an infinite loop, hit the square stop button in the toolbar (next to the Run button) or choose Interrupt from the Kernel menu. If you want the nuclear option (restart Python instead of just kill the loop), choose Restart from the Kernel menu instead.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Nesting\n",
    "\n",
    "Again, you can nest other loops or `if` statements inside of a `while` loop. Here's an example gets a bunch of random numbers and adds them together until `i` is greater than or equal to 5 and then tells you how many random numbers it needed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "i = 0\n",
    "count = 0\n",
    "\n",
    "# Keep going as long as i is less than 5\n",
    "while i < 5:\n",
    "    \n",
    "    # Get a random number\n",
    "    random_number = random.random() \n",
    "    \n",
    "    # Add the random number to i\n",
    "    i = i + random_number \n",
    "    \n",
    "    # Increment count\n",
    "    count = count + 1 # Increment count\n",
    "    \n",
    "# Print a message when the loop has finished (meaning i >= 5)\n",
    "print(f'It took {count} numbers to get to {i}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's a more complicated example that adds in an `if` statement so that in only uses random numbers greater that 0.5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 0\n",
    "count = 0\n",
    "total_count = 0\n",
    "\n",
    "# Keep going as long as i is less than 5\n",
    "while i < 5:\n",
    "    \n",
    "    # Get a random number\n",
    "    random_number = random.random() \n",
    "    \n",
    "    # Check if the random number is greater than 0.5 and increment\n",
    "    # i and count if it is\n",
    "    if random_number > 0.5:\n",
    "        i = i + random_number \n",
    "        count = count + 1\n",
    "        \n",
    "    # Notice that this line is not indented under the if statement,\n",
    "    # so total_count is always incremented, no matter what the value\n",
    "    # of random_number is.\n",
    "    total_count = total_count + 1\n",
    "    \n",
    "# Print a message when the loop has finished (meaning i >= 5)\n",
    "print(f'It took {count} numbers greater than 0.5 to get to {i}, but there were {total_count} random numbers generated.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
