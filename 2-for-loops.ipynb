{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generating lists with `range`\n",
    "\n",
    "Before you see how to use `for` loops, let's take a look at the [`range`](https://docs.python.org/3.6/library/stdtypes.html#typesseq-range) data type because it's commonly used with loops. (Just FYI, `range` in Python 2 is a function that returns a list. Both are used pretty much the same way, but the Python 3 method is less memory-intensive.) A `range` is a collection of numbers, but you can't see the numbers directly. We'll use `list()` to convert the `range` to a list so we can see the numbers, but there's usually no need to do this when using `range` in your code. \n",
    "\n",
    "If you give `range()` a single number, it'll contain numbers that start at 0 and go up to, **but do not include**, the number you provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(range(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you give `range()` two numbers, it starts at the first number and goes up to, but does not include, the second number you provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(range(3, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you give `range()` three numbers, the first two are the same as before, but the third tells it how much to increment by when creating the collection of numbers. So if you want to get every other number, use 2 as the third parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(range(0, 10, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "Use `range()` to generate this list of numbers:\n",
    "\n",
    "```\n",
    "[10, 15, 20, 25, 30, 35, 40, 45, 50]\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `for` loops\n",
    "\n",
    "The most common method for repetition is the `for` loop, which does something a certain number of times.\n",
    "\n",
    "A common use of `for` loops is to do the same thing to every item in a list. Here's the structure:\n",
    "\n",
    "```python\n",
    "for item in list_of_items:\n",
    "    # Statements that execute one time for each item contained in list_of_items.\n",
    "    statements\n",
    "```\n",
    "\n",
    "The `for` loop creates a temporary variable (`item` in the example) that gets each value in a list of values, one at a time. **You can call this variable whatever you want.** The first time through the loop the `item` variable contains the value of the first object in `list_of_items`. All of the code indented under the `for` line is run while the `item` variable is set to this first value in the list. This code can use the `item` variable, but it doesn't have to. \n",
    "\n",
    "After the code has been run once, the `item` variable gets assigned the second object in `list_of_items` and the process repeats. Again, all of the code indented under the `for` line is run while the `item` variable is set to the second value in the list. \n",
    "\n",
    "This process repeats until all objects in `list_of_items` have been iterated over.\n",
    "\n",
    "For example, if you have a list of county names, you could print each one out like this (in this case, the temporary variable is called `county`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "counties = ['Box Elder', 'Cache', 'Salt Lake', 'Utah', 'Weber']\n",
    "for county in counties:\n",
    "    print(f'{county} County')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the first time through the loop, the `county` variable is set to 'Box Elder' and it prints \"Box Elder County\". But the second time through the loop, the `county` variable is set to 'Cache', so it prints \"Cache County\". It keeps working its way through the list of county names until it gets to the last one, when `county` is set to 'Weber'. Then it stops. Even after it's done, the `county` variable still holds the last value ('Weber') unless you set it to something else, but most of the time you won't care about that.\n",
    "\n",
    "If you're having a hard time seeing what's happening, try playing with https://usu-python.gitlab.io/control-flow/for_loop_example.html and see if it helps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see how to use `range()` to loop a certain number of times. Say you want to do something 5 times. Using `range(5)` will get an object with 5 numbers in it (`[0, 1, 2, 3, 4]`), which you can use to do something 5 times, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = range(5)\n",
    "for i in numbers:\n",
    "    print(i, \"I'm loopy!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that you didn't have to use `list(range())` in this case. The only reason we used `list()` earlier was so we could print the numbers inside the `range`, because you can't print a `range` directly.\n",
    "\n",
    "The previous example used the value of the temporary `i` variable inside the indented code and printed it out along with \"I'm loopy!\", but you don't have to use that temporary variable if you don't want to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = range(5)\n",
    "for i in numbers:\n",
    "    print(\"I'm loopy!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's another example that uses the temporary `i` variable, but this time to do some math:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = range(5)\n",
    "print('numbers =', list(numbers))\n",
    "print('i j') # the quotes mean this prints the letters i and j (to use as column headers)\n",
    "for i in numbers:\n",
    "    j = i + i\n",
    "    print(i, j) # the lack of quotes means this prints the values stored in the i and j variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first time through that loop, `i` was set to the first value in `numbers`, so `i = 0`. Inside the loop, `j` was set to `i + i`, which is `0 + 0 = 0`. So when it printed `i` and `j`, it printed `0 0`. \n",
    "\n",
    "The second time through the loop, `i` was set to the second value in `numbers`, so `i = 1`. Then `j` was set to `1 + 1 = 2`, and it printed `1 2`.\n",
    "\n",
    "It continued like that until it got to the last value in `numbers`, when `i = 4` and `j = 4 + 4 = 8`.\n",
    "\n",
    "You could write that same loop with less code by getting rid of the `numbers` and `j` variables, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('i j')\n",
    "for i in range(5):\n",
    "    print(i, i + i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 2\n",
    "\n",
    "Use `range()` and a `for` loop to calculate and print out the square root of the numbers 0-9, **inclusive**. The function to get square root lives in the [`math` module](https://docs.python.org/3.6/library/math.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "import math\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's another example that adds up the first 10 numbers, starting at 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total = 0 # this will hold the total so far\n",
    "for i in range(1, 11): # 1-10\n",
    "    print(f'{total} + {i} = {total+i}')\n",
    "    total = total + i # add the number contained in i to the total\n",
    "print(f'grand total: {total}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First you set up a variable `total` to hold the sum as you added things up. This needs to start at 0. Then you used `range(1, 11)` instead of `range(10)`, because you wanted to add up the numbers 1-10 instead of 0-9. Then each time through the loop, you added `i` to the running sum contained in the `total` variable.\n",
    "\n",
    "The call to `print()` inside the loop is completely unnecessary, but I put it in there to help you see what was getting added up. \n",
    "\n",
    "Be aware that if you want to print something out from inside a loop, you need to make sure you use `print()`. You won't get any output if you don't."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 3\n",
    "\n",
    "Use `range()` and a `for` loop to find the factorial of 10. Remember that you get the factorial of a number by multiplying all of the integers up to and including that number together. So the factorial of 4 is `1 * 2 * 3 * 4 = 24`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's always important to check your code to make sure it's doing the correct thing. You can check your result by using the `factorial()` function (so yeah, you normally wouldn't calculate it with a loop, but you need practice using loops!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "math.factorial(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looping over two things at once\n",
    "\n",
    "What if you have two lists and you need to loop over them both at the same time? There are several ways to do it, and I'll show you three of them and you can decide which one makes more sense to you. These methods assume that both lists contain the same number of values.\n",
    "\n",
    "Here are the two lists we'll be working with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "counties = ['Box Elder', 'Cache', 'Salt Lake', 'Utah', 'Weber']\n",
    "cities = ['Brigham City', 'Logan', 'Salt Lake City', 'Provo', 'Ogden']\n",
    "print('counties:', counties)\n",
    "print('cities:', cities)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 1: `range`\n",
    "\n",
    "The first method we'll look at is to get the length of one of the lists and use `range` to iterate. This uses the fact that you can get items at a certain position in a list with brackets (remember that from the lists notebook last week?). So this prints out the first item in each list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(counties[0])\n",
    "print(cities[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you get the length of one of the lists and then use `range`, you get this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = len(counties)\n",
    "print(n)\n",
    "print(list(range(n)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you use the output from `range()` to set up your loop, you can use the loop variable (`i` in the example) to access values in the two lists. The first time through, `1 = 0`, so `counties[i]` is the same as `counties[0]`. Do the same to get the value at position `i` in the `cities` list, and then print out a string that uses them both."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = len(counties)\n",
    "for i in range(n):\n",
    "    county = counties[i] # Make sure you use a different name than counties for your new variable\n",
    "    city = cities[i]     # Same goes for the city\n",
    "    print(f'The county seat of {county} County is {city}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hopefully you noticed the comment in the code about using different variable names. If you had called the temporary variable inside the loop `counties` as well, it would have overwritten the `counties` variable from outside the loop (the one we set earlier in the notebook). If that gets overwritten, then nothing will work as you expect. **Always be careful not to overwrite variables that you need!**\n",
    "\n",
    "Here's the exact same thing, just written with less code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(len(counties)):\n",
    "    print(f'The county seat of {counties[i]} County is {cities[i]}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 2: `enumerate`\n",
    "\n",
    "The second method uses the [`enumerate()`](https://docs.python.org/3.6/library/functions.html#enumerate) function. You use this function with a list, and it will return an *iterable* of tuples. There's one tuple for each item in the original list. The second value in each tuple is the value from the original list, and the first value is the index of that value in the original list. It'll make more sense if you see it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(enumerate(counties))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like `range()`, `enumerate()` doesn't return something that can be printed, so we had to convert it to a list in order to view the contents. Here's an example that loops over the tuples returned by `enumerate()` and prints each one out on its own line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for item in enumerate(counties):\n",
    "    print(item)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In that example, the `item` variable was set to the tuple from `enumerate()` each time through the loop. But do you remember that you can set multiple variables at once? It works like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Remember that item is still set to (4, 'Weber') from the last code cell\n",
    "i, county = item\n",
    "print(i)\n",
    "print(county)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Conveniently, this will work when you set up a loop. So you can set `i` and `county` variables in your loop, instead of `item` like we did above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, county in enumerate(counties):\n",
    "    print(i, county)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you have your county name inside the loop, and you have an index you can use to get the city name from the `cities` list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, county in enumerate(counties):\n",
    "    city = cities[i]\n",
    "    print(f'The county seat of {county} County is {city}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or to cut down on the code a bit:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, county in enumerate(counties):\n",
    "    print(f'The county seat of {county} County is {cities[i]}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 3: `zip`\n",
    "\n",
    "The last method I'll show you also happens to be my favorite (and I'd be willing to bet that it's the favorite of most people who use Python on a regular basis). I showed you the other two methods first because I thought they might be easier to understand. If not, I apologize!\n",
    "\n",
    "Anyway, with this method you combine the original two lists into a \"list\" of tuples, and then iterate over that new list. I put quotes around list because `zip()` doesn't really return a list. Like `range()` and `enumerate()`, it returns an object that contains data that can be iterated over, but can't be viewed without turning it into a list first.\n",
    "\n",
    "To create the new list of tuples, use the [`zip()`](https://docs.python.org/3.6/library/functions.html#zip) function, like this (I printed out the original lists, too, just so you can see that the new one does indeed match the values up correctly)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('counties:', counties)\n",
    "print('cities:', cities)\n",
    "\n",
    "combined = zip(counties, cities)\n",
    "print('combined:', list(combined))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can set both the `county` and `city` variables when you make the loop, just like you set `i` and `county` when using `enumerate()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "combined = zip(counties, cities)\n",
    "for county, city in combined:\n",
    "    print(f'The county seat of {county} County is {city}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You don't need to create the `combined` variable, and can just use `zip()` while setting up your loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for county, city in zip(counties, cities):\n",
    "    print(f'The county seat of {county} County is {city}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 4\n",
    "\n",
    "Here are lists of people and their birth years. Use a `for` loop to print out each name and that person's age (assume they've all had their birthday this year, so you can just subtract their birth year from 2021). Your output will look something like this:\n",
    "\n",
    "```\n",
    "Bob 49\n",
    "Sally 31\n",
    "Tom 7\n",
    "Angie 40\n",
    "Linda 13\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "names = ['Bob', 'Sally', 'Tom', 'Angie', 'Linda']\n",
    "birth_years = [1971, 1989, 2013, 1980, 2007]\n",
    "\n",
    "# Add your code here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Nesting\n",
    "\n",
    "You can nest `if` statements inside of loops and vice versa (you can also nest loops within loops and `if` statements within other `if` statements).\n",
    "\n",
    "Here's an example that loops through the cities and counties but only prints info about Cache County."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for county, city in zip(counties, cities):\n",
    "    if county == 'Cache':\n",
    "        print(f'The county seat of {county} County is {city}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's an example that nests a loop inside an `if` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 7\n",
    "\n",
    "if n < 10:\n",
    "    # If n < 10 then print out all numbers less than n\n",
    "    for i in range(n):\n",
    "        print(i)\n",
    "else:\n",
    "    # Else if n >= 10, then print out even numbers less than n\n",
    "    for i in range(0, n, 2):\n",
    "        print(i)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change `n` to a number greater than 10 and see what happens."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "343.594px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
