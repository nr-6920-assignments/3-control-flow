{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Control flow\n",
    "\n",
    "Most scripts need to iterate over multiple items and evaluate conditions in order to determine what needs to be done. For example, a script could iterate over all files in a folder and check each one for the type of data it contains in order to decide if the file should be processed in some way. These processes are called *control flow*. This notebook will introduce you to `if-else` statements, which is the method used to make choices about what needs to be done.\n",
    "\n",
    "I've tried to describe the processes here, but I've also put together some HTML files that let you step through each line of code one at a time so you can see exactly what's happening. You can play with them at https://usu-python.gitlab.io/control-flow/, and they're also in the assignment's `examples` folder. \n",
    "\n",
    "**I realize that the concepts in the next few notebooks might be hard to understand, but don't just gloss over them if you don't get it right away. Almost everything you do in this class will require an understanding of control flow concepts, so your life will be much easier if you take the time to figure them out now instead of later. Please make use of the examples at https://usu-python.gitlab.io/control-flow/ and don't be afraid to ask questions.**\n",
    "\n",
    "\n",
    "# `if-else` statements\n",
    "\n",
    "Scripts are much more useful if they can perform different actions depending on a condition. This is where `if-else` statements come in. This is the basic structure, but only the first `if` condition is required:\n",
    "\n",
    "```python\n",
    "if first_condition is True:\n",
    "    # Run the indented code (first_set_of_statements) only \n",
    "    # if first_condition is True and then exit the if-else block. \n",
    "    # If first_condition is False, then it never gets to this\n",
    "    # indented code and checks the next condition instead.\n",
    "\n",
    "    first_set_of_statements\n",
    "\n",
    "elif second_condition is True:\n",
    "    # If first_condition was False, but second_condition is True, \n",
    "    # then run this indented code and then exit the if-else block.\n",
    "    # If second_condition is False, then it never gets to this\n",
    "    # indented code and checks the next condition instead.\n",
    "    \n",
    "    second_set_of_statements\n",
    "\n",
    "else:\n",
    "    # Else is a catch-all. This indented code will run if none of\n",
    "    # the earlier conditions were True and then it will exit the\n",
    "    # if-else block.\n",
    "    \n",
    "    else_set_of_statements\n",
    "```\n",
    "\n",
    "One and only one of these sets of statements will execute. Once a True condition is found, the code block under it is executed and the rest of the `if-else` statement is ignored.\n",
    "\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 76\n",
    "if a < 10:\n",
    "    print('a is less than 10')\n",
    "elif a < 50:\n",
    "    print('a is between 10 and 50')\n",
    "elif a < 100:\n",
    "    print('a is between 50 and 100')\n",
    "else:\n",
    "    print('a is greater than or equal to 100')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run that code and then try changing the value of `a` on the first line and rerunning it to see what happens. Do this with a variety of numbers and see if you can predict beforehand what will get printed.\n",
    "\n",
    "If you're still confused about what's going on, maybe https://usu-python.gitlab.io/control-flow/if_else_example.html will help.\n",
    "\n",
    "You can have the `if` statement all by itself, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 7\n",
    "if n == 7:\n",
    "    print('You guessed the lucky number!')\n",
    "print('Thanks for playing!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Notice that you used a double equal sign to check for equality. A single equal sign sets a variable, and a double checks to see if things are equal.**\n",
    "\n",
    "Change the value of `n` on the first line to some other number and see what happens when the `if` condition evaluates to `False`.\n",
    "\n",
    "The \"Thanks for playing\" message is outside of the `if-else` block, so it gets executed no matter what. If you didn't want the winners to see that message, you could put in an `else` block, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 7\n",
    "if n == 7:\n",
    "    print('You guessed the lucky number!')\n",
    "else:\n",
    "    print('Sorry, but thanks for playing.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, change the value of `n` and see what happens.\n",
    "\n",
    "If `n` is equal to 7, then it prints the message about the lucky number. If `n` is equal to anything other than 7, then the `else` clause makes it print the \"thanks for playing\" message. \n",
    "\n",
    "You can also have an `if` and an `elif`, but no `else`. In this case, the first message is printed if `n` is 7, and the second message is printed if `n` is 10. Nothing gets printed if `n` is anything else."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10\n",
    "if n == 7:\n",
    "    print('You guessed the lucky number!')\n",
    "elif n == 10:\n",
    "    print('You guessed the second lucky number!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, nothing will be printed because none of the conditions are True:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 70\n",
    "if n == 7:\n",
    "    print('You guessed the lucky number!')\n",
    "elif n == 10:\n",
    "    print('You guessed the second lucky number!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**You can only have one `if` block and one `else` block, but you can have as many `elif` blocks as you want.** This means that you can have as many conditions and possible outcomes as you want."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Whitespace, again\n",
    "\n",
    "Whitespace is very important in control flow, because that's how Python determines what should be inside of `if` statements and loops (you'll see loops in the next notebook). This next example will fail because Python expects at least one indented and executable line of code under an `if` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10\n",
    "if n == 7:\n",
    "    # There needs to be indented code here, but there's not.\n",
    "print('You guessed the lucky number!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You also might have an indentation error that Python doesn't catch for you, but will result in the wrong results. Here's an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10\n",
    "if n == 7:\n",
    "    print(\"It's your lucky day!\")\n",
    "print('You guessed the lucky number!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, you wanted both lines to print *only* if `n == 7`, but the second line always prints because the indentation is wrong. Indent that second line and rerun it to see what happens."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problems\n",
    "\n",
    "You know the answer to all of the calculations required in this problem, so you'll be able to tell if your code is doing the right thing or not. Make sure the code is printing out what you know the answer should be.\n",
    "\n",
    "The `%` operator returns the remainder of a division. For example, `27 / 5 = 5`, with a remainder of 2. You can use `%` to find that remainder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "27 % 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 1\n",
    "Think about how you could you use `%` to check if a number was divisible by 5. What would the remainder be and how would you get it? (Answer that question to yourself before trying to write code.) You want your check to return `True` if it is, and `False` if not. \n",
    "\n",
    "The problem is to check if `n` (the number 27) is divisible by 5. Right now it just needs to print out `True` or `False`, so all you need is the check. For example, if you were checking if `n` was equal to 7, the answer would be `n == 7` (no need for an `if` statement yet)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "n = 27\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 2\n",
    "Now put your solution to problem 1 into an `if-else` statement and have it print \"Divisible by 5\" if the number is divisible by 5, otherwise have it print \"Not divisible by 5\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "n = 27\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 3\n",
    "Copy your solution from problem 2 into this cell in order to check 25:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "n = 25\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 4\n",
    "Now change your code so that it prints \"Divisible by 5\" if the number is divisible by 5, \"Divisible by 2\" if the number is divisible by 2, and \"Not divisible by 5 or 2\" otherwise. Notice that some numbers might be divisible by both two and five. Treat those cases as if they're divisible by five but not by two. Check to see if the number 4 is divisible by 2 or 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "n = 4\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 5\n",
    "Copy your code from problem 4 and use it to check the number 10. Remember that if a number is divisible by both 5 and 2, then your code is supposed to treat it as if it's divisible by 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "n = 10\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 6\n",
    "Copy your code from problem 5 and use it to check the number 13."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "n = 13\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "343.594px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
