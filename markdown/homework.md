# Control flow homework

*Due Monday, February 8, at midnight*

Most scripts need to iterate over multiple items and evaluate conditions in order to determine what needs to be done. For example, a script could iterate over all files in a folder and check each one for the type of data it contains in order to decide if the file should be processed in some way. These processes are called *control flow*, and they're what you'll be learning about this week.

**I realize that the concepts this week might be hard to understand if you don't have scripting experience, but don't just gloss over them if you don't get it right away. These are some of the most important concepts you need to know in order to write scripts, and almost everything you do in this class will require an understanding of these ideas, so your life will be MUCH easier if you take the time to figure them out now instead of later. Please make use of the examples at https://usu-python.gitlab.io/control-flow/ and don't be afraid to ask questions.**

## Notebook comprehension questions

*16 points*

Read all four of these notebooks before attempting the problem notebooks!

Work through the following notebooks, do the problems in them, and turn them all in (even the ones without homework problems). Not only will I be looking at your answers to the problems, but I'll be looking to see that you ran all of the code (which hopefully means you read and understood the explanations!). Remember that you can change code to see what happens, which is an extremely good idea if you're not quite sure you understand what's going on.

As usual, you need to open Jupyter Notebook from the Start menu and open the notebooks from in there, because double-clicking on the files doesn't work.

Each problem is worth one point. You also get a point for running all of the code in the notebook.

Work through the notebooks in this order (don't forget to save them!):

- 1-if-else.ipynb (7 points)
- 2-for-loops.ipynb (5 points)
- 3-while-loops.ipynb (2 points)
- 4-list-comprehensions.ipynb (2 points)

When you're ready to turn them in, you can use GitHub Desktop, just like before (see the `5-finish.html` file from the first week if you don't remember how to do it). You can turn each one in as you finish it, or turn them in all at once. It doesn't matter.

## Script problems

*10 points each, for a total of 20 points*

*Remember that although these scripts may be worth less points than the notebooks, they're weighted much more heavily for your final grade.*

Usually I'll give you problem descriptions and you'll need to create a notebook for each problem, but I created the notebooks for you again this week. The problem description is at the top of the notebook, and then I added code cells with comments to walk you through the steps. You can work on these in any order:

- problem1.ipynb
- problem2.ipynb

Turn them in using GitHub Desktop, just like the other notebooks. You can turn each one in as you finish it, or turn them in all at once. It doesn't matter. Remember that you can stop GitLab from nagging you about the due date if you add `Closes #1` to your last commit for the assignment.
