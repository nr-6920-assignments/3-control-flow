# Control Flow

Most scripts need to iterate over multiple items and evaluate conditions in order to determine what needs to be done. For example, a script could iterate over all files in a folder and check each one for the type of data it contains in order to decide if the file should be processed in some way. These processes are called *control flow*, and they're what you'll be learning about this week.

**I realize that the concepts this week might be hard to understand if you don't have scripting experience, but don't just gloss over them if you don't get it right away. These are some of the most important concepts you need to know in order to write scripts, and almost everything you do in this class will require an understanding of these ideas, so your life will be MUCH easier if you take the time to figure them out now instead of later. Please make use of the examples at https://usu-python.gitlab.io/control-flow/ and don't be afraid to ask questions.**

See the [homework description](markdown/homework.md).
